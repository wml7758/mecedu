drop database teach;
create DATABASE teach;
use teach;

/*-------------------------字典-----------------------------------*/
drop table if exists teach_dict;
create table teach_dict (id int PRIMARY key auto_increment,
                            type_code varchar(10),
                            code varchar(10),
                            name VARCHAR(100),
                            ord int);
ALTER TABLE teach_dict ADD unique(type_code, name);

/*专业学制*/
insert into teach_dict(type_code,code, name,ord) values ('001','3', '三年制', 1);
insert into teach_dict(type_code,code, name,ord) values ('001','4', '四年制', 2);
/*专业类别*/
insert into teach_dict(type_code,code, name,ord) values ('002','01', '机械工程', 1);
insert into teach_dict(type_code,code, name,ord) values ('002','02', '机械设计制造', 2);
/*政治面貌*/
insert into teach_dict(type_code,code, name,ord) values ('003','01', '中共党员', 1);
insert into teach_dict(type_code,code, name,ord) values ('003','02', '中共预备党员', 2);
insert into teach_dict(type_code,code, name,ord) values ('003','03', '民主党派', 3);
insert into teach_dict(type_code,code, name,ord) values ('003','04', '群众', 4);
/*研究方向*/
insert into teach_dict(type_code,code, name,ord) values ('004','01', '物质与反物质作用力', 1);
/*授课学期*/
insert into teach_dict(type_code,code, name,ord) values ('005','01', '春季', 1);
insert into teach_dict(type_code,code, name,ord) values ('005','02', '秋季', 2);
/*授课学年*/
insert into teach_dict(type_code,code, name,ord) values ('006','1', '第一学年', 1);
insert into teach_dict(type_code,code, name,ord) values ('006','2', '第二学年', 2);
insert into teach_dict(type_code,code, name,ord) values ('006','3', '第三学年', 3);
insert into teach_dict(type_code,code, name,ord) values ('006','4', '第四学年', 4);
/*试题类型*/
insert into teach_dict(type_code,code, name,ord) values ('007','01', '选择题', 1);
insert into teach_dict(type_code,code, name,ord) values ('007','02', '填空题', 2);
insert into teach_dict(type_code,code, name,ord) values ('007','03', '计算题', 3);
insert into teach_dict(type_code,code, name,ord) values ('007','04', '名词解释', 4);
insert into teach_dict(type_code,code, name,ord) values ('007','05', '作图题', 5);
insert into teach_dict(type_code,code, name,ord) values ('007','06', '分析题', 6);
insert into teach_dict(type_code,code, name,ord) values ('007','07', '综合分析题', 7);
/*入学年份*/
insert into teach_dict(type_code,code, name,ord) values ('008','2018', '2018', 1);
insert into teach_dict(type_code,code, name,ord) values ('008','2019', '2019', 2);
insert into teach_dict(type_code,code, name,ord) values ('008','2020', '2020', 3);
insert into teach_dict(type_code,code, name,ord) values ('008','2021', '2021', 4);
/*课程性质*/
insert into teach_dict(type_code,code, name,ord) values ('009','01', '必修课', 1);
insert into teach_dict(type_code,code, name,ord) values ('009','02', '选修课', 2);
/*课程文档类型*/
insert into teach_dict(type_code,code, name,ord) values ('010','01', '图片文档', 1);
insert into teach_dict(type_code,code, name,ord) values ('010','02', '课程大纲', 2);
insert into teach_dict(type_code,code, name,ord) values ('010','03', '课程教案', 3);
insert into teach_dict(type_code,code, name,ord) values ('010','04', '课程计划', 4);
/*考试类别*/
insert into teach_dict(type_code,code, name,ord) values ('011','01', '考试', 1);
insert into teach_dict(type_code,code, name,ord) values ('011','02', '考查', 2);
insert into teach_dict(type_code,code, name,ord) values ('011','03', '期末考试', 3);
insert into teach_dict(type_code,code, name,ord) values ('011','04', '作业', 4);
insert into teach_dict(type_code,code, name,ord) values ('011','05', '报告', 5);
insert into teach_dict(type_code,code, name,ord) values ('011','06', '实验', 6);
insert into teach_dict(type_code,code, name,ord) values ('011','07', '考勤', 7);
insert into teach_dict(type_code,code, name,ord) values ('011','08', '讨论课', 8);
/*教学方式*/
insert into teach_dict(type_code,code, name,ord) values ('012','01', '操作', 1);
insert into teach_dict(type_code,code, name,ord) values ('012','02', '习题', 2);
insert into teach_dict(type_code,code, name,ord) values ('012','03', '讲授', 3);
insert into teach_dict(type_code,code, name,ord) values ('012','04', '实验', 4);
/*教师职称*/
insert into teach_dict(type_code,code, name,ord) values ('013','01', '教授', 1);
insert into teach_dict(type_code,code, name,ord) values ('013','02', '讲师', 2);
insert into teach_dict(type_code,code, name,ord) values ('013','03', '副教授', 3);
/*学科门类*/
insert into teach_dict(type_code,code, name,ord) values ('014','01', '哲学', 1);
insert into teach_dict(type_code,code, name,ord) values ('014','02', '经济学', 2);
insert into teach_dict(type_code,code, name,ord) values ('014','03', '法学', 3);
insert into teach_dict(type_code,code, name,ord) values ('014','04', '教育学', 4);
insert into teach_dict(type_code,code, name,ord) values ('014','05', '文学', 5);
insert into teach_dict(type_code,code, name,ord) values ('014','06', '历史学', 6);
insert into teach_dict(type_code,code, name,ord) values ('014','07', '理学', 7);
insert into teach_dict(type_code,code, name,ord) values ('014','08', '工学', 8);
insert into teach_dict(type_code,code, name,ord) values ('014','09', '农学', 9);
insert into teach_dict(type_code,code, name,ord) values ('014','10', '医学', 10);
insert into teach_dict(type_code,code, name,ord) values ('014','11', '军事学', 11);
insert into teach_dict(type_code,code, name,ord) values ('014','12', '管理学', 12);
insert into teach_dict(type_code,code, name,ord) values ('014','13', '艺术学', 13);

/*------------------------用户---------------------------------*/
drop table if exists mecedu_user;
create table mecedu_user(id int PRIMARY key auto_increment, name VARCHAR(100), sex varchar(10), code varchar(20), zhicheng varchar(50)
                         ,zhengzhi varchar(50), zhuanye varchar(50), yanjiu varchar(50), brith varchar(20),
                         type varchar(5) comment '员工类别' comment '1:教师，2学生，3系主任，4主管院长，5系统维护',
                         phone varchar(20),unit_code int, passwd varchar(100));
insert into mecedu_user(name,sex,code,zhicheng,zhengzhi,zhuanye,yanjiu, brith,type,phone,unit_code)
                values ('admin','1','admin','01','01','01','01','1982-11-10',1,'15523332222', '11'),
                        ('张三','1','001','01','01','01','01', '1988-02-12',1,'15523332222', '12'),
                        ('李四','0','002','01','01','01','01', '1988-02-12',1,'15523332222', '12');

/*------------------------教学单位-------------------------------*/
drop table if exists mecedu_teach_unit;
create table mecedu_teach_unit (id int primary key auto_increment,code varchar(50), name varchar(60));
alter table mecedu_teach_unit add unique (code);
insert into mecedu_teach_unit(code, name) values ('11', '理学院'),
                                                 ('12', '机械学院'),
                                                 ('13', '电气学院');

/*------------------------专业---------------------------------*/
drop table if exists mecedu_speciality;
create table mecedu_speciality(id int primary key auto_increment,
                            school_name varchar(50) comment '学院名称',
                            year varchar(10) comment '入学年份',
                            kind varchar(6) comment '专业类别',
                            subject varchar(10) comment '学科',
                            name varchar(60) comment '专业名称',
                            degree varchar(20) comment '学位',
                            xuezhi varchar(20) comment '专业学制'
                            );
insert into mecedu_speciality(school_name, year, kind, subject, name, degree, xuezhi) VALUES
    ('机械学院','2019','01','11','机械设计制造及其自动化','专科','3'),
    ('机械学院','2019','01','11','机械设计制造及其自动化1','学士','4');

/*------------------------课程---------------------------------*/
drop table if exists mecedu_course;
create table mecedu_course(id int primary key auto_increment,
                            pid int ,
                            speciality_id int comment '专业ID',
                            name varchar(100),
                            name_en varchar(200),
                            teach_time int,
                            experi_time int,
                            code varchar(100),
                            pc_time int,
                            adapt varchar(100) comment '适用对象',
                            xingzhi varchar(50),
                            credit int comment '学分',
                            type varchar(50) comment '考试类别',
                            all_time int comment '总学时',
                            is_core varchar(10),
                            year_no varchar(10) comment '授课学年',
                            period_no varchar(10) comment '授课学期',
                            unit_code varchar(50) comment '授课单位',
                            ord int comment '顺序号'
                            );
insert into mecedu_course(pid,speciality_id,name,name_en,teach_time,experi_time,code,pc_time,adapt,xingzhi,credit,type,all_time,is_core,year_no,period_no,unit_code, ord)
values(null,1,'思想政治理论',null,null,null,null,null,null,null,null,null,null,null,null,null,null,1),
        (1,1,'思想道德修养与法律基础','Ideological Morality and Legal Basis',40,8,'G0004A2625',8,'通识教育课程','01','3','01',48,'0','1','01','12',1),
      (1,1,'中国近代史纲要','Conspectus of Chinese Modern History',40,8,'G0003A2625',8,'通识教育课程','01','3','01',48,'0','1','01','12',2),
      (1,1,'马克思主义原理概论','The Conspectus of Fundamental Theory of Marxist',40,8,'G0002A2625',8,'通识教育课程','01','3','01',48,'0','1','01','12',3),

      (null,1,'学科专业基础',null,null,null,null,null,null,null,null,null,null,null,null,null,null,2),
      (5,1,'理论力学','Ideological Morality and Legal Basis',40,8,'G0004A2625',8,'通识教育课程','01','3','01',48,'0','1','01','13',1),
      (5,1,'热工基础','Conspectus of Chinese Modern History',40,8,'G0003A2625',8,'通识教育课程','01','3','01',48,'0','1','01','12',2),
      (5,1,'机械原理','The Conspectus of Fundamental Theory of Marxist',40,8,'G0002A2625',8,'通识教育课程','01','3','01',48,'0','1','01','12',3);

/*------------------------课程教师关系---------------------------------*/
drop table if exists mecedu_course_user;
create table mecedu_course_user(id int primary key auto_increment,
                                speciality_id int comment '专业ID',
                                course_code varchar(100),
                                user_id int,
                                role int comment '1责任教师，2授课教师'
                                );
alter table mecedu_course_user add unique (speciality_id,course_code,user_id,role);
insert into mecedu_course_user(speciality_id, course_code, user_id, role) values
                                (1,'G0004A2625',2,1),(1,'G0004A2625',2,2),
                                (1,'G0003A2625',3,1),(1,'G0003A2625',3,2),(1,'G0003A2625',2,2);

/*-----------------------------------课程叶子节点视图------------------------------*/
drop view if exists mecedu_course_leaf;
create view mecedu_course_leaf as select t.* from mecedu_course t where not exists (select 1 from mecedu_course as m where m.pid=t.id);

/*-----------------------指标------------------------------------*/
drop table if exists mecedu_indicator;
create table mecedu_indicator(id int primary key auto_increment,
                            pid int,
                            speciality_id int,
                            code varchar(200) not null unique,
                            content varchar(1000)
                            );
insert into mecedu_indicator(pid,speciality_id,code,content)
                        values (null,1,'毕业要求1.工程知识','：能够掌握本专业所需的数学、自然科学、工程基础和专业知识'),
                               (1,1,'1-1','能够掌握数学与自然科学相关知识，并能够理解其如何用英语复杂工程问题的描述'),
                               (1,1,'1-2','能够掌握理论力学，材料力学，热流体，电工电子学，材料科学基础等基础领域的工程基础知识');
/*------------------------------------指标叶子节点---------------------------------*/
drop view if exists mecedu_indicator_leaf;
create view mecedu_indicator_leaf as select * from mecedu_indicator as a where not exists (select 1 from mecedu_indicator as b where b.pid=a.id);

/*-----------------------课程Matrix---------------------------------*/
drop table if exists mecedu_course_matrix;
create table mecedu_course_matrix(id int primary key auto_increment,
                            code_course varchar(100),
                            code_indicator varchar(100),
                            rel varchar(10) comment '关联关系 H|M|L',
                            weight float,
                            speciality_id int);
alter table mecedu_course_matrix add unique (code_course,code_indicator, speciality_id);
insert into mecedu_course_matrix(code_course,code_indicator,rel,weight,speciality_id)
                        values ('G0004A2625','1-1','H',0.5,1),
                               ('G0003A2625','1-2','H',0.3,1),
                               ('G0002A2625','1-1','H',0.5,1),('G0002A2625','1-2','H',0.7,1);

/*-----------------------课程目标----------------------------------*/
drop table if exists mecedu_course_target;
create table mecedu_course_target(id int primary key auto_increment,
                                  speciality_id int,
                                  course_code varchar(100) not null,
                                  title varchar(200),
                                  content varchar(1000),
                                  remark varchar(200)
                                  );
insert into mecedu_course_target(course_code, title,content, remark, speciality_id)
                            values('G0004A2625','目标41','内容1','xxxxx',1),('G0004A2625','目标42','内容2','YYYYYY',1),
                                  ('G0003A2625','目标31','内容2','YYYYYY',1),('G0003A2625','目标32','内容2','YYYYYY',1),
                                  ('G0002A2625','目标21','内容2','YYYYYY',1),('G0002A2625','目标22','内容2','YYYYYY',1);

/*-----------------------课程目标Matrix----------------------------------*/
drop table if exists mecedu_target_matrix;
create table mecedu_target_matrix(id int primary key auto_increment,
                                  target_id int not null,
                                  indicator_code varchar(100),
                                  weight float);
alter table mecedu_target_matrix add unique (target_id, indicator_code);
insert into mecedu_target_matrix(target_id,indicator_code,weight)
                            values(1,'1-1',0.8),(2,'1-1',0.2),
                                  (3,'1-2',0.4),(4,'1-2',0.6),
                                  (5,'1-1',0.5),(5,'1-2',0.3),(6,'1-1',0.5),(6,'1-2',0.7);

/*-----------------------知识模块----------------------------------*/
drop table if exists mecedu_course_module;
create table mecedu_course_module(id int primary key auto_increment,
                                    course_code varchar(100),
                                    speciality_id int,
                                    name varchar(100) not null,
                                    score float comment '建议分值',
                                    ord int comment '排序',
                                    time_teach float,
                                    time_question float,
                                    time_pc float,
                                    time_test float,
                                    time_practice float,
                                    time_discuss float,
                                    content varchar(500),
                                    claim varchar(500) comment '教学要求'
                                    );
insert into mecedu_course_module(course_code,speciality_id,name,score,ord,time_teach,time_question,time_pc,time_test,time_practice,time_discuss,content,claim)
                    values('G0004A2625',1,'模块1',10,1,8,8,8,8,8,8,'思想道德修养与法律基础-模块1','要求1'),
                          ('G0004A2625',1,'模块2',20,1,8,8,8,8,8,8,'思想道德修养与法律基础-模块2','要求1'),

                          ('G0003A2625',1,'模块1',30,1,8,8,8,8,8,8,'中国近代史纲要-模块1','要求1'),
                          ('G0003A2625',1,'模块2',40,1,8,8,8,8,8,8,'中国近代史纲要-模块2','要求1'),
                          ('G0003A2625',1,'模块3',50,1,8,8,8,8,8,8,'中国近代史纲要-模块3','要求1'),

                          ('G0002A2625',1,'模块1',60,1,8,8,8,8,8,8,'马克思主义原理概论-模块1','要求1'),
                          ('G0002A2625',1,'模块2',70,1,8,8,8,8,8,8,'马克思主义原理概论-模块2','要求1'),
                          ('G0002A2625',1,'模块3',80,1,8,8,8,8,8,8,'马克思主义原理概论-模块3','要求1'),
                          ('G0002A2625',1,'模块4',90,1,8,8,8,8,8,8,'马克思主义原理概论-模块4','要求1');
/*-----------------------知识模块矩阵---------------------------------*/
drop table if exists mecedu_module_matrix;
create table mecedu_module_matrix(id int primary key auto_increment,
                                  module_id int not null,
                                  target_id int not null,
                                  weight float);
alter table mecedu_module_matrix add unique (module_id,target_id);
insert into mecedu_module_matrix(module_id, target_id,weight) values
                                (1,1,0.3),(1,2,0.4),(2,1,0.7),(2,2,0.6),
                                (3,3,0.5),(3,4,0.5),(4,3,0.2),(4,4,0.3),(5,3,0.3),(5,4,0.2);

/*-------------------------考核评价---------------------------------*/
drop table if exists mecedu_course_assess;
create table mecedu_course_assess(id int primary key auto_increment,
                                  speciality_id int,
                                  course_code varchar(100) not null,
                                  segment varchar(10) comment '考核环节',
                                  content varchar(300));
alter table mecedu_course_assess add unique (speciality_id,course_code,segment);
insert into mecedu_course_assess(speciality_id, course_code, segment, content)
                            values(1,'G0004A2625','01','XXXXXX'),
                                  (1,'G0004A2625','02','XXXXXX'),
                                  (1,'G0004A2625','03','XXXXXX'),

                                  (1,'G0003A2625','03','XXXXXX'),
                                  (1,'G0003A2625','04','XXXXXX');

/*-------------------------考核评价Matrix---------------------------------*/
drop table if exists mecedu_assess_matrix;
create table mecedu_assess_matrix(id int primary key auto_increment,
                                  assess_id int not null,
                                  target_id int not null,
                                  weight float);
alter table mecedu_assess_matrix add unique (assess_id, target_id);
insert into mecedu_assess_matrix(assess_id, target_id, weight)
                                values (1,1,0.5),(1,2,0.3),(2,1,0.1),(2,2,0.2),(3,1,0.4),(3,2,0.5),
                                       (4,3,0.5),(4,4,0.3),(5,3,0.5),(5,4,0.7);

/*-------------------------作业库---------------------------------*/
drop table if exists mecedu_homework;
create table mecedu_homework(id int primary key auto_increment,
                                  module_id int not null,
                                  content varchar(1000),
                                  answer varchar(600));
insert into mecedu_homework(module_id, content, answer) VALUES
                          (1,'xxxx','yyyy'),(1,'adfas','casd'),(2,'fdsfads','fas'),(3,'fsdf','fasdff'),(4,'dff','ss');

/*-------------------------试题库---------------------------------*/
drop table if exists mecedu_question;
create table mecedu_question(id int primary key auto_increment,
                             type varchar(10) comment '试题类型',
                             difficulty varchar(10) comment '难度系数',
                             intro varchar(200) comment '简介',
                             content varchar(1000) comment '试题内容',
                             answer varchar(600) comment '试题答案');
insert into mecedu_question (type, difficulty, intro, content, answer) VALUES
        ('01','55','简介', 'kadfasdf', 'ssssss'),
        ('02','xxxx','简介1', 'ssss', 'ssssss'),
        ('03','BB','简介3', 'xxx', 'gggg');
/*-------------------------试题模块分值表---------------------------------*/
drop table if exists mecedu_question_module;
create table mecedu_question_module(id int primary key auto_increment,
                             question_id int comment '试题ID',
                             module_id int comment '所属模块',
                             score float comment '分值');
insert into mecedu_question_module (question_id, module_id, score) VALUES
                                  (1,1,2),(1,2,3),
                                  (2,1,1),
                                  (3,2,5);

/*-------------------------班级---------------------------------*/
drop table if exists mecedu_class_info;
create table mecedu_class_info(id int primary key auto_increment,
                               speciality_id int comment '专业',
                               serial varchar(10) comment '序号',
                               code varchar(10) comment '代码',
                               name varchar(200) comment '名称',
                               people varchar(1000) comment '人数');
alter table mecedu_class_info add unique(speciality_id,code);
insert into mecedu_class_info(speciality_id, code, serial, name, people) values
                                    (1,'001','1','班级一','20'),(1,'002','2','班级二','20'),
                                    (1,'003','3','班级三','20'),(1,'004','4','班级四','20');

/*------------------------课程、教师、班级----------------------*/
drop table if exists mecedu_course_class_user;
create table mecedu_course_class_user(id int primary key auto_increment,
                               course_id int,
                               code varchar(100),
                               class_ids varchar(200),
                               user_ids varchar(200)
                               );
alter table mecedu_course_class_user add unique(course_id,code);
insert into mecedu_course_class_user(course_id, code, class_ids, user_ids) VALUES
                            (1,'001','1','1');

/*------------------------试卷-------------------------------*/
drop table if exists mecedu_paper;
create table mecedu_paper(id int primary key auto_increment,
                                      course_id int,
                                      name varchar(100),
                                      update_time datetime default now() comment '修改时间',
                                      user_id int comment '修改人'
                         );
alter table mecedu_paper add unique(course_id,name);
insert into mecedu_paper(course_id, name, user_id) VALUES (1,'试卷一',1),(1,'试卷二',1),(1,'试卷三',1);

/*------------------------试卷<-->试题-------------------------------*/
drop table if exists mecedu_paper_question;
create table mecedu_paper_question(id int primary key auto_increment,
                          paper_id int,
                          question_id int,
                          ord int
                        );
alter table mecedu_paper_question add unique(paper_id,question_id);
insert into mecedu_paper_question(paper_id, question_id) VALUES (1,1),(1,2),(1,3);
