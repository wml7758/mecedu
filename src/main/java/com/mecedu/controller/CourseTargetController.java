package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.CourseTargetMapper;
import com.mecedu.mode.CourseTarget;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseTargetController {

    @Autowired
    private CourseTargetMapper targetMapper;

    @PostMapping("/target/add")
    public RetResult add(@RequestParam("specialityId") Integer specialityId,
                         @RequestParam("courseCode") String courseCode,
                         @RequestParam("title") String title,
                         String content,
                         String remark){

        CourseTarget target = new CourseTarget();
        target.setSpecialityId(specialityId);
        target.setCourseCode(courseCode);
        target.setTitle(title);
        target.setContent(content);
        target.setRemark(remark);

        Integer row = targetMapper.insert(target);
        if(row>0){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/target/query")
    public RetResult query(@RequestParam("specialityId") Integer specialityId,
                         @RequestParam("courseCode") String courseCode){

        QueryWrapper<CourseTarget> wrapper = new QueryWrapper<>();
        wrapper.eq("speciality_id", specialityId);
        wrapper.eq("course_code", courseCode);
        List<CourseTarget> list = targetMapper.selectList(wrapper);

        return RetResponse.makeOKRsp(list);
    }

    @PostMapping("/target/del")
    public RetResult del(@RequestParam("id") Integer id){

        Integer row = targetMapper.deleteById(id);
        if(row == 1) {
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }

    @PostMapping("/target/update")
    public RetResult update(CourseTarget target){
        Assert.notNull(target.getId(),"ID不能为空");
        Assert.notNull(target.getTitle(), "标题不能为空");

        Integer row = targetMapper.updateById(target);
        if(row == 1) {
            return RetResponse.makeOKRsp(target);
        }else{
            return RetResponse.makeErrRsp("更新失败");
        }
    }
}
