package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.fasterxml.jackson.databind.util.JSONPObject;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.CourseMapper;
import com.mecedu.mapper.CourseMatrixMapper;
import com.mecedu.mapper.IndicatorMapper;
import com.mecedu.mode.Course;
import com.mecedu.mode.CourseMatrix;
import com.mecedu.mode.Indicator;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
public class CourseMatrixController {

    @Autowired
    private CourseMatrixMapper courseMatrixMapper;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private IndicatorMapper indicatorMapper;

    @GetMapping("/course/matrix/show")
    public RetResult getMatrix(@RequestParam("specialityId") Integer specialityId) {
        List<Course> courses = courseMapper.findLeaf(specialityId);
        courses.get(0).getTargets();
        List<Indicator> indicators = indicatorMapper.findLeaf(specialityId);

        QueryWrapper query = new QueryWrapper();
        query.eq("speciality_id", specialityId);
        List<CourseMatrix> matrix = courseMatrixMapper.selectList(query);

        /*Object[] d = courses.stream().map(course -> {
            List<String> l = new ArrayList<>(indicators.size());

            indicators.stream().forEach(indicator -> {
                Optional<CourseMatrix> opt = matrix.stream().filter(m -> m.getCodeCourse().equals(course.getCode())).filter(m -> m.getCodeIndicator().equals(indicator.getCode())).findFirst();
                if (opt.isPresent()) {
                    l.add(opt.get().getWeight());
                } else {
                    l.add("");
                }
            });

            return l;
        }).toArray();*/
        MatrixResult result = new MatrixResult();
        result.courses = courses;
        result.indicators = indicators;
        result.matrices = matrix;

        return RetResponse.makeOKRsp(result);
    }

    @PostMapping("/course/matrix/addOrDelOrUpdate")
    public RetResult<Object> add(@RequestParam("specialityId") Integer specialityId,
                                 @RequestParam("courseCode") String courseCode,
                                 @RequestParam("indicatorCode") String indicatorCode,
                                 Float weight, /* (H/M/L) ,为空时就是删除指标 */
                                 @RequestParam("rel") String rel
    ) {
        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("speciality_id", specialityId);
        queryWrapper.eq("code_course", courseCode);
        queryWrapper.eq("code_indicator", indicatorCode);
        List<CourseMatrix> list = courseMatrixMapper.selectList(queryWrapper);

        if (StringUtils.isEmpty(weight)) {
            if(list.size() > 0) {
                courseMatrixMapper.deleteById(list.get(0).getId());
            }
        } else {
            if(list.size()>0){//更新
                UpdateWrapper<CourseMatrix> updateWrapper= new UpdateWrapper<>();
                updateWrapper.eq("id", list.get(0).getId())
                        .set("weight", weight)
                        .set("rel", rel);
                courseMatrixMapper.update(null, updateWrapper);
            }else{//新增
                CourseMatrix courseMatrix = new CourseMatrix();
                courseMatrix.setCodeCourse(courseCode);
                courseMatrix.setCodeIndicator(indicatorCode);
                courseMatrix.setSpecialityId(specialityId);
                courseMatrix.setWeight(weight);
                courseMatrixMapper.insert(courseMatrix);
            }
        }

        return RetResponse.makeOKRsp();
    }

    class MatrixResult {
        List<Course> courses;
        List<Indicator> indicators;
        List<CourseMatrix> matrices;
    }
}
