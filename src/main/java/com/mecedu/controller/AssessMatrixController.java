package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.*;
import com.mecedu.mode.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class AssessMatrixController {

    @Autowired
    private CourseTargetMapper courseTargetMapper;

    @Autowired
    private CourseAssessMapper courseAssessMapper;

    @Autowired
    private AssessMatrixMapper assessMatrixMapper;

    @PostMapping("/assessmatrix/saveOrUpdate")
    public RetResult add(AssessMatrix assessMatrix){
        Assert.notNull(assessMatrix.getTargetId(), "目标ID能为空");
        Assert.notNull(assessMatrix.getAssessId(), "评估ID不能为空");

        QueryWrapper<AssessMatrix> query = new QueryWrapper<>();
        query.eq("target_id", assessMatrix.getTargetId())
             .eq("assess_id", assessMatrix.getAssessId());

        Integer row = assessMatrixMapper.selectCount(query);
        if(row == 0){
            assessMatrixMapper.insert(assessMatrix);
        }else{
            UpdateWrapper<AssessMatrix> updateWrapper = new UpdateWrapper<>();
            updateWrapper.eq("target_id", assessMatrix.getTargetId())
                         .eq("assess_id", assessMatrix.getAssessId())
                         .set("weight", assessMatrix.getWeight());
            assessMatrixMapper.update(null, updateWrapper);
        }
        return RetResponse.makeOKRsp();
    }

    @GetMapping("/assess/matrix/show")
    public RetResult show(@RequestParam("specialityId") Integer specialityId,
                          @RequestParam("courseCode") String courseCode) {
        //查询Assess
        QueryWrapper<CourseAssess> assessQuery = new QueryWrapper();
        assessQuery.eq("speciality_id", specialityId);
        assessQuery.eq("course_code", courseCode);
        List<CourseAssess> assesses = courseAssessMapper.selectList(assessQuery);

        //查询目标
        QueryWrapper<CourseTarget> targetQuery = new QueryWrapper<>();
        targetQuery.eq("speciality_id", specialityId);
        targetQuery.eq("course_code", courseCode);
        List<CourseTarget> targets = courseTargetMapper.selectList(targetQuery);

        //模块指标
        QueryWrapper<AssessMatrix> matrixQuery = new QueryWrapper<>();
        matrixQuery.in("assess_id", assesses.stream().map(s->s.getId()));
        matrixQuery.in("target_id", targets.stream().map(s->s.getId()));

        List<AssessMatrix> matrixs = assessMatrixMapper.selectList(matrixQuery);

        Result r = new Result();
        r.matrixs= matrixs;
        r.assesses = assesses;
        r.targets = targets;

        return RetResponse.makeOKRsp(r);
    }


    private class Result{
        List<CourseAssess> assesses;
        List<CourseTarget> targets;
        List<AssessMatrix> matrixs;
    }
}
