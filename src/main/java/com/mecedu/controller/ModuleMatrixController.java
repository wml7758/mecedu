package com.mecedu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.api.R;
import com.google.gson.Gson;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.CourseModuleMapper;
import com.mecedu.mapper.ModuleMatrixMapper;
import com.mecedu.mapper.CourseTargetMapper;
import com.mecedu.mode.CourseModule;
import com.mecedu.mode.CourseTarget;
import com.mecedu.mode.ModuleMatrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ModuleMatrixController {

    @Autowired
    private ModuleMatrixMapper moduleMatrixMapper;

    @Autowired
    private CourseModuleMapper courseModuleMapper;

    @Autowired
    private CourseTargetMapper courseTargetMapper;

    @PostMapping("/module/matrix/saveOrUpdate")
    public RetResult saveOrUpdate(ModuleMatrix moduleMatrix){
        Assert.notNull(moduleMatrix.getModuleId(), "模块ID不能为空");
        Assert.notNull(moduleMatrix.getTargetId(), "目标ID不能为空");

        QueryWrapper<ModuleMatrix> queryWrapper = new QueryWrapper();
        queryWrapper.eq("module_id", moduleMatrix.getModuleId())
                    .eq("target_id", moduleMatrix.getTargetId());
        Integer count = moduleMatrixMapper.selectCount(queryWrapper);
        if(count>0){
            UpdateWrapper<ModuleMatrix> updateWrapper = new UpdateWrapper();
            updateWrapper.eq("module_id", moduleMatrix.getModuleId())
                        .eq("target_id", moduleMatrix.getTargetId())
                        .set("weight", moduleMatrix.getWeight());
            moduleMatrixMapper.update(null, updateWrapper);
        }else{
            moduleMatrixMapper.insert(moduleMatrix);
        }

        return RetResponse.makeOKRsp();
    }

    @GetMapping("/module/matrix/show")
    public RetResult show(@RequestParam("specialityId") Integer specialityId,
                          @RequestParam("courseCode") String courseCode) {

        //查询module
        QueryWrapper<CourseModule> moduleQuery = new QueryWrapper();
        moduleQuery.eq("speciality_id", specialityId);
        moduleQuery.eq("course_code", courseCode);
        List<CourseModule> modules = courseModuleMapper.selectList(moduleQuery);

        //查询目标
        QueryWrapper<CourseTarget> targetQuery = new QueryWrapper<>();
        targetQuery.eq("speciality_id", specialityId);
        targetQuery.eq("course_code", courseCode);
        List<CourseTarget> targets = courseTargetMapper.selectList(targetQuery);

        //模块指标
        QueryWrapper<ModuleMatrix> matrixQuery = new QueryWrapper<>();
        matrixQuery.in("module_id", modules.stream().map(s->s.getId()));
        matrixQuery.in("target_id", targets.stream().map(s->s.getId()));

        List<ModuleMatrix> matrixs = moduleMatrixMapper.selectList(matrixQuery);

        Result r = new Result();
        r.matrixs = matrixs;
        r.modules = modules;
        r.targets = targets;

        return RetResponse.makeOKRsp(r);
    }


    private class Result{
        List<CourseModule> modules;
        List<CourseTarget> targets;
        List<ModuleMatrix> matrixs;
    }
}


