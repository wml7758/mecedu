package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.HomeWorkMapper;
import com.mecedu.mode.Homework;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class HomeworkController {

    @Autowired
    private HomeWorkMapper homeWorkMapper;

    @PostMapping("/homework/add")
    public RetResult add(Homework homework){
        Assert.notNull(homework.getModule_id(), "模块ID不能为空");

        homeWorkMapper.insert(homework);

        return RetResponse.makeOKRsp();
    }

    @GetMapping("/home/work/query")
    public RetResult query(@RequestParam("keyword") String keyword){

        QueryWrapper<Homework> queryWrapper = new QueryWrapper<>();
        queryWrapper.like("content", keyword).or().like("answer", keyword);

        List<Homework> list = homeWorkMapper.selectList(queryWrapper);
        return RetResponse.makeOKRsp(list);
    }
}
