package com.mecedu.controller.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.QuestionMapper;
import com.mecedu.mode.test.Question;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class QuestionController {

    @Autowired
    private QuestionMapper questionMapper;

    @PostMapping("/question/add")
    public RetResult add(Question question){
        //TODO:xxx
        //Assert.notNull(question.getModuleId(),"moduleId不能为空");
        Assert.notNull(question.getType(),"试题类型不能为空");
        Integer row = questionMapper.insert(question);
        if(row>0){
            return RetResponse.makeOKRsp(question);
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @GetMapping("/question/query")
    public RetResult query(@RequestParam("specialityId") Integer specialityId,
                            @RequestParam("courseCode") String courseCode,
                           String keyword){

        QueryWrapper<Question> queryWrapper = new QueryWrapper();
        if(!StringUtils.isEmpty(keyword)) {
            queryWrapper.like("intro", keyword)
                    .or().like("content", keyword)
                    .or().like("answer", keyword);
        }

        List<Question> list = questionMapper.query(specialityId, courseCode, keyword);
        return RetResponse.makeOKRsp(list);
    }

    @PostMapping("/question/update")
    public RetResult update(Question question){
        Assert.notNull(question.getId(),"id不能为空");
        Integer row = questionMapper.updateById(question);
        if(row>0){
            return RetResponse.makeOKRsp(question);
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @GetMapping("/question/get")
    public RetResult get(@RequestParam("id") Integer id){
        Assert.notNull(id,"id不能为空");
        Question q = questionMapper.selectById(id);
        return RetResponse.makeOKRsp(q);
    }

    @PostMapping("/question/del")
    public RetResult del(@RequestParam("id") Integer id){
        Assert.notNull(id,"id不能为空");
        Integer row = questionMapper.deleteById(id);
        if(row >0) {
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("删除失败");
        }
    }
}
