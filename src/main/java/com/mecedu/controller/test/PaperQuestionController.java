package com.mecedu.controller.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.test.PaperQuestionMapper;
import com.mecedu.mode.test.PaperQuestion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class PaperQuestionController {

    @Autowired
    private PaperQuestionMapper mapper;

    @PostMapping("/paper/addQuestion")
    public RetResult addQuestion(@RequestParam("paperId") Integer paperId,
                                 @RequestParam("questionId") Integer questionId){

        PaperQuestion paperQuestion = new PaperQuestion();
        paperQuestion.setPaperId(paperId);
        paperQuestion.setQuestionId(questionId);

        Integer row = mapper.insert(paperQuestion);
        if(row>0) {
            return RetResponse.makeOKRsp(paperQuestion);
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/paper/delQuestion")
    public RetResult delQuestion(@RequestParam("paperId") Integer paperId,
                                 @RequestParam("questionId") Integer questionId){

        QueryWrapper<PaperQuestion> wrapper = new QueryWrapper<>();
        wrapper.eq("paper_id",paperId);
        wrapper.eq("question_id", questionId);
        Integer row = mapper.delete(wrapper);
        if(row>0) {
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }

    @GetMapping("/paper/question/summary")
    public RetResult delQuestion(@RequestParam("paperId") Integer paperId){

        List<Map<String, Object>> list = mapper.pageSummarize(paperId);
        return RetResponse.makeOKRsp(list);
    }

    @PostMapping("/paper/question/order")
    public RetResult orderQuestion(@RequestParam("paperId") Integer paperId,
                                   @RequestParam("questionId") Integer questionId,
                                   @RequestParam("order") Integer order){

        UpdateWrapper<PaperQuestion> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("paper_id", paperId);
        updateWrapper.eq("question_id", questionId);
        updateWrapper.set("ord", order);

        Integer row = mapper.update(null ,updateWrapper);
        if(row>0) {
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("排序失败");
        }
    }
}
