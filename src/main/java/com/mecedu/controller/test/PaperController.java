package com.mecedu.controller.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.SessionUtil;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.CourseMapper;
import com.mecedu.mapper.SpecialityMapper;
import com.mecedu.mapper.test.PaperMapper;
import com.mecedu.mode.Course;
import com.mecedu.mode.Speciality;
import com.mecedu.mode.User;
import com.mecedu.mode.test.Paper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class PaperController {

    @Autowired
    private PaperMapper mapper;

    @PostMapping("/page/add")
    public RetResult add(@RequestParam("courseId") Integer courseId,
                         @RequestParam("name") String name){
        User user = SessionUtil.getCurrentUser();

        Paper paper = new Paper();
        paper.setName(name);
        paper.setCourseId(courseId);
        paper.setUserId(user.getId());

        Integer row = mapper.insert(paper);
        if(row>0) {
            return RetResponse.makeOKRsp(paper);
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @GetMapping("/paper/query")
    public RetResult query(@RequestParam("courseId") Integer courseId,
                           @RequestParam("keyword") String keyword,
                           Page page){

        QueryWrapper<Paper> wrapper = new QueryWrapper<>();
        wrapper.eq("course_id", courseId);
        wrapper.like("name", keyword);

        Page p = mapper.selectPage(page, wrapper);
        return RetResponse.makeOKRsp(ConstanceUtils.toJson(p));
    }

    @Autowired
    private CourseMapper courseMapper;
    @Autowired
    private SpecialityMapper specialityMapper;


    @GetMapping("/paper/info")
    public RetResult info(@RequestParam("paperId") Integer paperId){

        Paper paper = mapper.selectById(paperId);
        Course course = courseMapper.selectById(paper.getCourseId());
        Speciality speciality = specialityMapper.selectById(course.getSpecialityId());

        return RetResponse.makeOKRsp();
    }
}
