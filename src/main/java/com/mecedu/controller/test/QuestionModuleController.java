package com.mecedu.controller.test;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.test.QuestionModuleMapper;
import com.mecedu.mode.test.Question;
import lombok.experimental.Accessors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class QuestionModuleController {

    @Autowired
    private QuestionModuleMapper mapper;

    @GetMapping("/question/module/query")//根据模块查询
    public RetResult query2(@RequestParam("moduleId") Integer moduleId,
                            @RequestParam("type") String type, Page page){
        Page p = mapper.selectByModule(page, moduleId, type);
        return RetResponse.makeOKRsp(ConstanceUtils.toJson(p));
    }

    /*@GetMapping("/question/selected")//试卷中已选中题目
    public RetResult selected(@RequestParam("pageId") Integer pageId){

        QueryWrapper<Question> queryWrapper = new QueryWrapper();
        queryWrapper.eq("module_id", moduleId);
        queryWrapper.eq("type", type);

        Page p = questionMapper.selectPage(page, queryWrapper);
        return RetResponse.makeOKRsp(ConstanceUtils.toJson(p));
    }*/
}
