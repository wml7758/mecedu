package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.DictMapper;
import com.mecedu.mode.Dict;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import retrofit2.http.QueryMap;

import java.util.List;

@RestController
public class DictController {

    @Autowired
    private DictMapper dictMapper;

    @GetMapping("/dict/type/list")
    public RetResult getTypeList(){
        return RetResponse.makeOKRsp(ConstanceUtils.typeCodeName);
    }

    @GetMapping("/dict/list")
    public RetResult list(String keyword, Page page) {
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();

        if(!StringUtils.isEmpty(keyword)) {
            wrapper .like("name", keyword);
            Object[] array=ConstanceUtils.typeCodeName.entrySet().stream().filter(e->e.getValue().indexOf(keyword)!=-1).map(e->e.getKey()).toArray();
            wrapper.in("type_code", array);
        }

        IPage<Dict> p = dictMapper.selectPage(page, wrapper);
        p.getRecords().stream().forEach(s->s.setTypeName(ConstanceUtils.typeCodeName.get(s.getTypeCode())));
        return RetResponse.makeOKRsp(ConstanceUtils.toJson(p));
    }

    @GetMapping("/dict/item/query")
    public RetResult<Dict> queryByName(@RequestParam("id") Integer id){
        QueryWrapper<Dict> wrapper = new QueryWrapper<>();
        Dict dict = dictMapper.selectById(id);
        dict.setTypeName(ConstanceUtils.typeCodeName.get(dict.getTypeCode()));
        return RetResponse.makeOKRsp(dict);
    }

    @GetMapping("/dict/type/query")
    public RetResult query(@RequestParam("typeCode") String typeCode){
        QueryWrapper<Dict> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("type_code",typeCode);
        List<Dict> list = dictMapper.selectList( queryWrapper);
        list.stream().forEach(s->s.setTypeName(ConstanceUtils.typeCodeName.get(s.getTypeCode())));

        return RetResponse.makeOKRsp(list);
    }

    @PostMapping("/dict/add")
    public RetResult add(Dict dict){
        Integer row = dictMapper.insert(dict);
        if(row == 1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("插入失败");
        }
    }

    @PostMapping("/dict/update")
    public RetResult update(Dict dict){
        Assert.notNull(dict.getId(),"ID不能为空");
        Integer row = dictMapper.updateById(dict);
        if(row == 1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("插入失败");
        }
    }

    @PostMapping("/dict/del")
    public RetResult update(Integer id){
        Integer row = dictMapper.deleteById(id);
        if(row == 1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }
}
