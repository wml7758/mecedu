package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.CourseMapper;
import com.mecedu.mode.ClassInfo;
import com.mecedu.mode.Course;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseController {

    @Autowired
    private CourseMapper mapper;

    @GetMapping("/course/tree")
    public RetResult<Object> tree(@RequestParam("specialityId") String specialityId){
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("speciality_id", specialityId);
        List<Course> list = mapper.selectList(wrapper);
        if(list.size() >0 ){
            return RetResponse.makeOKRsp().setData(list);
        }else{
            return RetResponse.makeErrRsp("查看失败");
        }
    }

    @GetMapping("/course/query")
    public RetResult<Object> query(@RequestParam("specialityId") String specialityId,
                                    @RequestParam("keyword") String keyword){
        QueryWrapper wrapper = new QueryWrapper();
        wrapper.eq("speciality_id", specialityId);
        wrapper.like("name", keyword);

        List<Course> list = mapper.selectList(wrapper);
        if(list.size() >0 ){
            return RetResponse.makeOKRsp().setData(list);
        }else{
            return RetResponse.makeErrRsp("查看失败");
        }
    }

    @GetMapping("/course/get")
    public RetResult<Course> get(@RequestParam("specialityId") String specialityId,
                                   @RequestParam("code") String code){
        QueryWrapper<Course> wrapper = new QueryWrapper();
        wrapper.eq("speciality_id", specialityId);
        wrapper.eq("code", code);
        Course c = mapper.selectOne(wrapper);
        return RetResponse.makeOKRsp(c);
    }

    @PostMapping("/course/add")
    public RetResult<Object> courseAdd(Course course){

       int row = mapper.insert(course);

       if(row >0 ){
           return RetResponse.makeOKRsp();
       }else{
           return RetResponse.makeErrRsp("添加失败");
       }
    }

    @PostMapping("/course/update")
    public RetResult<Object> update(Course course){
        int row = mapper.updateById(course);

        if(row >0 ){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/course/updateName")
    public RetResult<Course> updateName(@RequestParam("id") Integer id,@RequestParam("name") String name){
        UpdateWrapper<Course> wrapper = new UpdateWrapper();
        wrapper.eq("id", id).set("name",name);

        int row = mapper.update(null, wrapper);

        if(row >0 ){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/course/del")
    @Transactional
    public RetResult<Course> del(Integer id){

        int row = delCourseTree(id);

        if(row >0 ){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }

    @PostMapping("/course/move")
    @Transactional
    public RetResult<Course> del(@RequestParam("sourceId") Integer sourceId, //待移动节点的ID
                                 @RequestParam("parentId") Integer parentId,//父节点的ID
                                 @RequestParam("ord") Integer ord){ //移动后的序号

        UpdateWrapper<Course> orderUpdate = new UpdateWrapper<>();
        orderUpdate.ge("ord", ord).setSql(true, "ord=ord+1");
        mapper.update(null, orderUpdate);

        UpdateWrapper<Course> updateWrapper = new UpdateWrapper<>();
        updateWrapper.eq("id",sourceId).set("pid", parentId).set("ord", ord);
        mapper.update(null, updateWrapper);

        return RetResponse.makeOKRsp();
    }

    private int delCourseTree(Integer id){
        List<Integer> children = mapper.getChildren(id);

        for(Integer cid: children){
            delCourseTree(cid);
        }

        return mapper.deleteById(id);
    }
}
