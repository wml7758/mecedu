package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.CourseModuleMapper;
import com.mecedu.mode.CourseModule;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseModuleController {

    @Autowired
    private CourseModuleMapper courseModuleMapper;

    @PostMapping("/course/module/add")
    public RetResult add(CourseModule module){
        Assert.notNull(module.getName(),"模块名字不能为空");
        Assert.notNull(module.getCourseCode(),"课程编码不能为空");
        Assert.notNull(module.getSpecialityId(),"专业不能为空");

        courseModuleMapper.insert(module);

        return RetResponse.makeOKRsp();
    }

    @PostMapping("/course/module/query")
    public RetResult query(@RequestParam("courseCode") String courseCode,
                           @RequestParam("specialityId") Integer specialityId){
        Assert.notNull(courseCode,"课程代码不能为空");
        Assert.notNull(specialityId, "专业ID不能为空");

        QueryWrapper<CourseModule> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("course_code", courseCode)
                .eq("speciality_id", specialityId);
        List<CourseModule> list = courseModuleMapper.selectList(queryWrapper);

        return RetResponse.makeOKRsp(list);
    }

    @PostMapping("/course/module/update")
    public RetResult update(CourseModule courseModule){
        Assert.notNull(courseModule.getId(),"ID不能为空");
        Assert.notNull(courseModule.getName(), "名称不能为空");

        Integer row = courseModuleMapper.updateById(courseModule);
        if(row ==1) {
            return RetResponse.makeOKRsp(courseModule);
        }else{
            return RetResponse.makeErrRsp("更新失败");
        }
    }

    @PostMapping("/course/module/del")
    public RetResult query(@RequestParam("id") Integer id){
        Integer row = courseModuleMapper.deleteById(id);
        if(row == 1) {
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("删除失败");
        }
    }

}
