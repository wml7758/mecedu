package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.Query;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.SpecialityMapper;
import com.mecedu.mode.Speciality;
import org.apache.ibatis.annotations.Param;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SpecialityController {

    @Autowired
    private SpecialityMapper mapper;

    @PostMapping("/speciality/add")
    public RetResult add(Speciality speciality){

        Assert.notNull(speciality.getName(),"专业名称不能为空");
        Assert.notNull(speciality.getYear(),"专业年份不能为空");

        int row = mapper.insert(speciality);

        return RetResponse.makeOKRsp();
    }

    @PostMapping("/speciality/query")
    public RetResult query(@RequestParam("name") String name, Page page){
        QueryWrapper<Speciality> queryWrapper = new QueryWrapper<>();
        if(!StringUtils.isEmpty(name)){
            queryWrapper.like("name", name);
        }
        IPage<Speciality> p = mapper.selectPage(page, queryWrapper);

        return RetResponse.makeOKRsp(ConstanceUtils.toJson(p));
    }

    @GetMapping("/speciality/queryById")
    public RetResult queryById(@RequestParam("id") Integer id){
        Speciality p = mapper.selectByIdFull(id);
        p.getKindDict().setTypeName(ConstanceUtils.typeCodeName.get(p.getKindDict().getTypeCode()));
        p.getSubjectDict().setTypeName(ConstanceUtils.typeCodeName.get(p.getSubjectDict().getTypeCode()));
        p.getXuezhiDict().setTypeName(ConstanceUtils.typeCodeName.get(p.getXuezhiDict().getTypeCode()));
        return RetResponse.makeOKRsp(p);
    }


    @PostMapping("/speciality/update")
    public RetResult update(Speciality speciality){

        Assert.notNull(speciality.getId(),"ID不能为空");

        int row = mapper.updateById(speciality);
        if(row >0) {
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }

    @PostMapping("/speciality/del")
    public RetResult del(@RequestParam("id") Integer id){

        Assert.notNull(id,"ID不能为空");

        int row = mapper.deleteById(id);
        if(row >0) {
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }
}
