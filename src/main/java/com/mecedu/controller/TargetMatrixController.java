package com.mecedu.controller;


import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.*;
import com.mecedu.mode.Course;
import com.mecedu.mode.CourseTarget;
import com.mecedu.mode.Indicator;
import com.mecedu.mode.TargetMatrix;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
public class TargetMatrixController {

    @Autowired
    private TargetMatrixMapper targetMatrixMapper;

    @Autowired
    private CourseMapper courseMapper;

    @Autowired
    private CourseTargetMapper targetMapper;

    @Autowired
    private IndicatorMapper indicatorMapper;

    @Autowired
    private CourseMatrixMapper courseMatrixMapper;


    @PostMapping("/target/matrix/add")
    public RetResult add(@RequestParam("targetId") Integer targetId,
                         @RequestParam("indicatorCode") String indicatorCode,
                         @RequestParam("weight") String weight
                         ){

        TargetMatrix matrix = new TargetMatrix();
        matrix.setTargetId(targetId);
        matrix.setIndicatorCode(indicatorCode);
        matrix.setWeight(Float.valueOf(weight));

        int row = targetMatrixMapper.insert(matrix);

        if(row>0) {
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/target/matrix/update")
    public RetResult update(@RequestParam("targetMatrixId") Integer targetMatrixId,
                         @RequestParam("weight") String weight
    ){

        UpdateWrapper<TargetMatrix> updateWrapper = new UpdateWrapper();
        updateWrapper.eq("id", targetMatrixId).set("weight", weight);

        int row = targetMatrixMapper.update(null, updateWrapper);

        if(row>0) {
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @GetMapping("/target/matrix/show")
    public RetResult show(@RequestParam("courseId") Integer courseId){

        Course course = courseMapper.selectById(courseId);

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("speciality_id", course.getSpecialityId());
        queryWrapper.eq("course_code", course.getCode());
        List<CourseTarget> targets = targetMapper.selectList(queryWrapper);

        List<Indicator> indicators = courseMatrixMapper.getIndicators(course.getCode(),course.getSpecialityId());

        Object[] targetMatrixIds =  targets.stream().map(target->target.getId()).toArray();

        Map<String,List<TargetMatrix>> map = new HashMap<>();
        QueryWrapper w = new QueryWrapper();
        w.in("target_id", targetMatrixIds);
        List<TargetMatrix> targetMatrices = targetMatrixMapper.selectList(w);

        ShowResult result = new ShowResult();
        result.course = course;
        result.targets = targets;
        result.indicators = indicators;
        result.matrix = targetMatrices;

        return RetResponse.makeOKRsp(result);
    }

    class ShowResult {
        Course course;
        List<CourseTarget> targets;
        List<Indicator> indicators;
        List<TargetMatrix> matrix;
    }

}
