package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.CourseAssessMapper;
import com.mecedu.mode.CourseAssess;
import org.omg.PortableInterceptor.INACTIVE;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseAssessController {

    @Autowired
    private CourseAssessMapper courseAssessMapper;

    @PostMapping("/course/assess/add")
    public RetResult add(CourseAssess courseAssess){
        Assert.notNull(courseAssess.getCourseCode(),"课程代码不能为空");
        Assert.notNull(courseAssess.getSpecialityId(),"专业不能为空");
        Assert.notNull(courseAssess.getSegment(),"考核环节不能为空");

        Integer row = courseAssessMapper.insert(courseAssess);
        if(row >0){
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/course/assess/query")
    public RetResult query(@RequestParam("courseCode") String courseCode,
                           @RequestParam("specialityId") Integer specialityId){
        Assert.notNull(courseCode,"课程代码不能为空");
        Assert.notNull(specialityId,"课程年份不能为空");

        QueryWrapper<CourseAssess> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("speciality_id", specialityId)
                .eq("course_code", courseCode);
        List<CourseAssess> list = courseAssessMapper.selectList(queryWrapper);
        return RetResponse.makeOKRsp(list);
    }

    @PostMapping("/course/assess/update")
    public RetResult query(CourseAssess assess){
        Assert.notNull(assess.getId(),"ID不能为空");
        Assert.notNull(assess.getSegment(),"环节不能为空");

        Integer row = courseAssessMapper.updateById(assess);
        if(row >0) {
            return RetResponse.makeOKRsp(assess);
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/course/assess/del")
    public RetResult del(@RequestParam("id") Integer id){
        Assert.notNull(id,"ID不能为空");
        Integer row = courseAssessMapper.deleteById(id);
        if(row>=1) {
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }
}
