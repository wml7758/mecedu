package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.TeachUnitMapper;
import com.mecedu.mode.TeachUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeachUnitController {

    @Autowired
    private TeachUnitMapper unitMapper;


    @GetMapping("/teachunit/list")
    public IPage<TeachUnit> selectUnit(String keyword, Page page){
        QueryWrapper<TeachUnit> wrapper = new QueryWrapper<>();

        if(!StringUtils.isEmpty(keyword)) {
            wrapper .likeRight("name", keyword)
                    .or()
                    .likeLeft("name",keyword);
        }

        IPage<TeachUnit> p = unitMapper.selectPage(page, wrapper);
        return p;
    }

    @PostMapping("/teachunit/add")
    public RetResult<Object> insert(TeachUnit unit){
        int row = unitMapper.insert(unit);

        if(row ==1){
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/teachunit/update")
    public RetResult<Object> update(TeachUnit unit){
        Assert.notNull(unit.getId(), "没有ID");

        int row = unitMapper.updateById(unit);

        if(row>0){
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("更新失败");
        }
    }

    @PostMapping("/teachunit/del")
    public RetResult<Object> delete(Integer id){
        Assert.notNull(id, "没有ID");

        int row = unitMapper.deleteById(id);

        if(row>0){
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("删除失败");
        }
    }

}
