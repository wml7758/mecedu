package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.UserMapper;
import com.mecedu.mode.ClassInfo;
import com.mecedu.mode.User;
import org.apache.tomcat.util.security.MD5Encoder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class UserController {

    @Autowired
    private UserMapper userMapper;

    @PostMapping("/user/add")
    @Transactional
    public RetResult delete(User user){
        Integer row = userMapper.insert(user);

        if(row == 1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("插入失败");
        }
    }

    @PostMapping("/user/update")
    public RetResult update(User user){
        Integer row = userMapper.updateById(user);
        if(row == 1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("插入失败");
        }
    }

    @GetMapping("/user/list")
    public IPage queryByName(String keyword, Page page){
        QueryWrapper<User> wrapper = new QueryWrapper<>();

        if(!StringUtils.isEmpty(keyword)) {
            wrapper .likeRight("name", keyword)
                    .or()
                    .likeLeft("name",keyword);
        }

        IPage<User> p = userMapper.selectPage(page, wrapper);
        return p;
    }

    //更新密码
    @PostMapping("/user/acc/update")
    public RetResult updatePwd(@RequestParam("id") Long id,
                               @RequestParam("passwd") String passwd){
        passwd = ConstanceUtils.encodePwd(passwd);

        Integer row = userMapper.updateAcc(id, passwd);

        if(row == 1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("更新失败");
        }
    }

    //查询账号
    @PostMapping("/user/acc/query")
    public RetResult queryAcc(@RequestParam("code")String code, @RequestParam("passwd") String passwd){
        passwd = ConstanceUtils.encodePwd(passwd);
        Integer count = userMapper.queryAccount(code, passwd);
        if(count > 0) {
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("无此用户");
        }
    }
}
