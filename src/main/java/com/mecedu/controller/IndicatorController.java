package com.mecedu.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.IndicatorMapper;
import com.mecedu.mode.Indicator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.stream.Collectors;

@RestController
public class IndicatorController {

    @Autowired
    private IndicatorMapper mapper;

    @GetMapping("/indicator/tree")
    public RetResult<Object> getAll(@RequestParam("specialityId") Integer specialityId){

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.eq("speciality_id", specialityId);
        List<Indicator> list = mapper.selectList(queryWrapper);

        return RetResponse.makeOKRsp().setData(list);
    }

    @GetMapping("/indicator/query")
    public RetResult<Object> query(String keyword){

        QueryWrapper queryWrapper = new QueryWrapper();
        queryWrapper.select("id");
        if(!StringUtils.isEmpty(keyword)) {
            queryWrapper.like("content", keyword);
        }

        List<Indicator> list = mapper.selectList(queryWrapper);
        List<Integer> ids = list.stream().map(s->s.getId()).collect(Collectors.toList());
        return RetResponse.makeOKRsp().setData(ids);
    }

    @PostMapping("/indicator/add")
    public RetResult<Object> add(Indicator indicator){
        int row = mapper.insert(indicator);
        if(row>0){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("插入失败");
        }
    }

    @PostMapping("/indicator/update")
    public RetResult<Object> update(@RequestParam("id") Integer id , String code, String content){

        UpdateWrapper<Indicator> wrapper = new UpdateWrapper();
        wrapper.eq("id", id).set("code",code).set("content",content);
        int row = mapper.update(null, wrapper);

        if(row>0){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("更新失败");
        }
    }

    @PostMapping("/indicator/del")
    public RetResult<Object> del(@RequestParam("id") Integer id){
        int row = mapper.deleteById(id);
        if(row>0){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }
}
