package com.mecedu.controller.teach;

import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.CourseMapper;
import com.mecedu.mode.Course;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TeachTaskController {

    @Autowired
    private CourseMapper courseMapper;


    @GetMapping("/teachtask/query")
    public RetResult query(Integer year, String period, Page page) {

        if (ConstanceUtils.DICT_PEROID_SPRING.equals(period)) {//如果是春季，还是上一学年。
            year -= 1;
        }

        IPage<Course> p = courseMapper.teachTask(page, year, period);
        Gson parser = new Gson();
        return RetResponse.makeOKRsp(parser.toJson(p));
    }


}
