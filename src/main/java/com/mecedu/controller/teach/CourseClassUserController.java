package com.mecedu.controller.teach;

import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonPrimitive;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.teach.CourseClassUserMapper;
import com.mecedu.mode.teach.CourseClassUser;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CourseClassUserController {

    @Autowired
    private CourseClassUserMapper mapper;


    @PostMapping("/course/user/class/add")
    public RetResult<Object> add(
                            @RequestParam("courseId") Integer courseId,
                            @RequestParam("code") String code){
        CourseClassUser courseClassUser = new CourseClassUser();
        courseClassUser.setCode(code);
        courseClassUser.setCourseId(courseId);
        courseClassUser.setClassIds("[]");
        courseClassUser.setUserIds("[]");

        Integer row = mapper.insert(courseClassUser);
        if(row ==1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("插入失败");
        }
    }

    @PostMapping("/course/user/class/updateUser")
    public RetResult<Object> updateUser(@RequestParam("id") Integer id,
                                 @RequestParam("userId") Integer userId,
                                 @RequestParam("op") String op /*操作类型 0删除，1增加*/
                                ){
        CourseClassUser courseClassUser = mapper.selectById(id);
        String userIds = courseClassUser.getUserIds();
        Gson gson = new Gson();
        JsonArray array = gson.fromJson(userIds, JsonArray.class);
        if("1".equals(op)){
            array.add(userId);
        }else if("0".equals(op)){
            JsonPrimitive s = new JsonPrimitive(userId);
            array.remove(s);
        }

        UpdateWrapper<CourseClassUser> update = new UpdateWrapper<>();
        update.eq("id", id).set("user_ids", array.toString());
        Integer row = mapper.update(null, update);
        if(row ==1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("列新失败");
        }
    }

    @PostMapping("/course/user/class/updateClass")
    public RetResult<Object> updateClass(@RequestParam("id") Integer id,
                                 @RequestParam("classId") Integer classId,
                                 @RequestParam("op") String op /*操作类型 0删除，1增加*/
    ){
        CourseClassUser courseClassUser = mapper.selectById(id);
        String classIds = courseClassUser.getUserIds();
        Gson gson = new Gson();
        JsonArray array = gson.fromJson(classIds, JsonArray.class);
        if("1".equals(op)){
            array.add(classId);
        }else if("0".equals(op)){
            JsonPrimitive s = new JsonPrimitive(classId);
            array.remove(s);
        }

        UpdateWrapper<CourseClassUser> update = new UpdateWrapper<>();
        update.eq("id", id).set("class_ids", array.toString());
        Integer row = mapper.update(null, update);
        if(row ==1){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("列新失败");
        }
    }


    @Test
    public void ss(){
        Gson json = new Gson();
        JsonArray array = json.fromJson("[]", JsonArray.class);
        array.add("as");
        array.add(55);
        array.add(66);
        JsonPrimitive s = new JsonPrimitive(55);
        array.remove(s);
        System.out.println(array);
    }
}
