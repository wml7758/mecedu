package com.mecedu.controller.teach;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.teach.CourseUserMapper;
import com.mecedu.mode.User;
import com.mecedu.mode.teach.CourseUser;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class CourseUserController {

    @Autowired
    private CourseUserMapper courseUserMapper;

    /**
     * 课程，授课单位下所有教师
     * @param specialityId
     * @param courseCode
     * @return
     */
    @GetMapping("/course/user/all")
    public RetResult queryByCourseAll(@RequestParam("specialityId") Integer specialityId, @RequestParam("courseCode") String courseCode){
        List<User> users = courseUserMapper.queryByCourseAll(specialityId, courseCode, ConstanceUtils.USER_TYPE_TEACHER);
        return RetResponse.makeOKRsp(users);
    }

    /**
     * 课程的所有教师
     */
    @GetMapping("/course/user/checked")
    public RetResult queryByCourseChecked(@RequestParam("specialityId") Integer specialityId, @RequestParam("courseCode") String courseCode){
        QueryWrapper<CourseUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("speciality_id", specialityId)
                .eq("course_code", courseCode);
        List<CourseUser> courseUsers = courseUserMapper.selectList(queryWrapper);
        return RetResponse.makeOKRsp(courseUsers);
    }

    /**
     * 课程可用的教师
     */
    @GetMapping("/course/user/checked2")
    public RetResult queryByCourseChecked2(@RequestParam("specialityId") Integer specialityId, @RequestParam("courseCode") String courseCode){
        QueryWrapper<CourseUser> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("speciality_id", specialityId)
                .eq("course_code", courseCode);
        List<User> courseUsers = courseUserMapper.queryByCourseChecked2(specialityId, courseCode,ConstanceUtils.COURSE_TEACHER_ROLE_TEACH);
        return RetResponse.makeOKRsp(courseUsers);
    }

    @PostMapping("/course/user/add")
    public RetResult add(CourseUser courseUser){
        Assert.notNull(courseUser.getSpecialityId(),"专业不能为空");
        Assert.notNull(courseUser.getCourseCode(),"课程代码不能为空");
        Assert.notNull(courseUser.getUserId(),"userId不能为空");
        Assert.notNull(courseUser.getRole(),"角色不能为空");

        Integer row = courseUserMapper.insert(courseUser);

        if(row>0){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @PostMapping("/course/user/del")
    public RetResult del(CourseUser courseUser){
        Assert.notNull(courseUser.getSpecialityId(),"专业不能为空");
        Assert.notNull(courseUser.getCourseCode(),"课程代码不能为空");
        Assert.notNull(courseUser.getUserId(),"userId不能为空");
        Assert.notNull(courseUser.getRole(),"角色不能为空");

        QueryWrapper<CourseUser> queryWrapper = new QueryWrapper();
        queryWrapper.eq("speciality_id",courseUser.getSpecialityId())
                .eq("course_code",courseUser.getCourseCode())
                .eq("user_id", courseUser.getUserId())
                .eq("role", courseUser.getRole());
        Integer row = courseUserMapper.delete(queryWrapper);
        if(row >0 ){
            return RetResponse.makeOKRsp();
        }else{
            return RetResponse.makeErrRsp("删除失败");
        }
    }
}
