package com.mecedu.controller.teach;

import com.alibaba.druid.support.json.JSONUtils;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.mecedu.configuration.ConstanceUtils;
import com.mecedu.configuration.response.RetResponse;
import com.mecedu.configuration.response.RetResult;
import com.mecedu.mapper.ClassInfoMapper;
import com.mecedu.mode.ClassInfo;
import org.apache.ibatis.javassist.util.HotSwapAgent;
import org.apache.tomcat.util.bcel.Const;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class ClassInfoController {

    @Autowired
    private ClassInfoMapper classInfoMapper;

    @PostMapping("/teach/class/add")
    public RetResult add(ClassInfo classInfo){
        Assert.notNull(classInfo.getSpecialityId(),"专业不能为空");
        Assert.notNull(classInfo.getCode(),"代码不能为空");

        Integer row = classInfoMapper.insert(classInfo);
        if(row == 1) {
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("添加失败");
        }
    }

    @GetMapping("/teach/class/get")
    public RetResult<ClassInfo> get(@RequestParam("id") Integer id){
        ClassInfo classInfo = classInfoMapper.selectById(id);
        return RetResponse.makeOKRsp(classInfo);
    }

    @PostMapping("/teach/class/del")
    public RetResult del(@RequestParam("id") Integer id){
        Integer row = classInfoMapper.deleteById(id);
        if(row == 1){
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("删除失败");
        }
    }

    @PostMapping("/teach/class/update")
    public RetResult del(ClassInfo classInfo){
        Integer id = classInfo.getId();
        Assert.notNull(id, "班级ID不能为空");

        Integer row = classInfoMapper.updateById(classInfo);
        if(row == 1){
            return RetResponse.makeOKRsp();
        }else {
            return RetResponse.makeErrRsp("更新失败");
        }
    }

    @GetMapping("/teach/class/query")
    public RetResult<?> queryPage(@RequestParam("keyword") String keyword, Page page){
        QueryWrapper<ClassInfo> queryWrapper = new QueryWrapper<>();

        if(!StringUtils.isEmpty(keyword)) {
            queryWrapper.like("name", keyword);
        }

        IPage<ClassInfo> p = classInfoMapper.selectPage(page, queryWrapper);

        return RetResponse.makeOKRsp(ConstanceUtils.toJson(p));
    }

    /**
     * 选择课程可用的班级
     */
    @GetMapping("/teach/class/bycourse")
    public RetResult<?> queryClass(@RequestParam("specialityId") Integer specialityId){
        QueryWrapper<ClassInfo> queryWrapper = new QueryWrapper<>();
        queryWrapper.eq("speciality_id", specialityId);
        List<ClassInfo> list = classInfoMapper.selectList(queryWrapper);
        return RetResponse.makeOKRsp(list);
    }
}
