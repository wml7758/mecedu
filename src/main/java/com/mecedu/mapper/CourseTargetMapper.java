package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.CourseTarget;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseTargetMapper extends BaseMapper<CourseTarget> {

}
