package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.CourseModule;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseModuleMapper extends BaseMapper<CourseModule> {
}
