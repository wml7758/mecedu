package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.mode.Course;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CourseMapper extends BaseMapper<Course> {

    @Select("select id from mecedu_course where pid=${id}")
    public List<Integer> getChildren(@Param("id") Integer id);

    @Select({"select * from mecedu_course_leaf c where speciality_id=${specialityId}"})
    public List<Course> findLeaf(@Param("specialityId") int specialityId);

    @Select("select c.* from mecedu_speciality as s inner join mecedu_course as c on s.id = c.speciality_id " +
            "where s.year+c.year_no-1=${year} and c.period_no=#{peroid}")
    public IPage<Course> teachTask(Page<Course> page, @Param("year") Integer year, @Param("peroid") String peroid);
}
