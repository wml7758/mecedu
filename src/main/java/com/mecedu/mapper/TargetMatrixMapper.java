package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.TargetMatrix;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface TargetMatrixMapper extends BaseMapper<TargetMatrix> {
}
