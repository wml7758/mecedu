package com.mecedu.mapper.teach;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.mode.User;
import com.mecedu.mode.teach.CourseUser;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CourseUserMapper extends BaseMapper<CourseUser> {

    @Select("select * from mecedu_user as u where u.type=#{type} and EXISTS (select 1 from mecedu_course as c where c.speciality_id=${specialityId} and c.code=#{courseCode} and u.unit_code=c.unit_code)")
    List<User> queryByCourseAll(@Param("specialityId") Integer specialityId,
                                @Param("courseCode") String courseCode,
                                @Param("type") String type);



    @Select("select * from mecedu_user as u where EXISTS (SELECT 1 from mecedu_course_user as cu where u.id=cu.user_id and cu.speciality_id=#{specialityId} and cu.course_code=#{courseCode} and cu.role=#{role}) ")
    List<User> queryByCourseChecked2(@Param("specialityId") Integer specialityId,
                                    @Param("courseCode") String courseCode,
                                    @Param("role") Integer role);
}
