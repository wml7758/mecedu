package com.mecedu.mapper.teach;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.teach.CourseClassUser;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseClassUserMapper extends BaseMapper<CourseClassUser> {
}
