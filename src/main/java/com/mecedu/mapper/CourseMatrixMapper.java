package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.CourseMatrix;
import com.mecedu.mode.Indicator;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface CourseMatrixMapper extends BaseMapper<CourseMatrix> {
    @Select("select * from mecedu_indicator as a where a.speciality_id=${speciality_id} and exists " +
            "(select 1 from mecedu_course_matrix b where b.speciality_id=${speciality_id} and b.code_course=#{courseCode} and b.weight='H' and b.code_indicator=a.code)")
    List<Indicator> getIndicators(@Param("courseCode") String courseCode, @Param("speciality_id") Integer speciality_id);
}
