package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.CourseAssess;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface CourseAssessMapper extends BaseMapper<CourseAssess> {
}
