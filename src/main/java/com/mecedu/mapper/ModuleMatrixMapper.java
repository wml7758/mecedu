package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.ModuleMatrix;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ModuleMatrixMapper extends BaseMapper<ModuleMatrix> {

}
