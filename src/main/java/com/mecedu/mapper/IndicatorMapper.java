package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.Course;
import com.mecedu.mode.Indicator;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface IndicatorMapper extends BaseMapper<Indicator> {

    @Select("select * from mecedu_indicator_leaf as a where a.speciality_id=#{specialityId}")
    public List<Indicator> findLeaf(@Param("specialityId") int specialityId);
}
