package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.ClassInfo;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ClassInfoMapper extends BaseMapper<ClassInfo> {
}
