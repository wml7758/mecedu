package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.test.Question;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

@Mapper
public interface QuestionMapper extends BaseMapper<Question> {

    @Select("<script>" +
            "select q.* from mecedu_question as q " +
            "inner join mecedu_course_module as m on q.module_id=m.id " +
            "where m.speciality_id=#{specialityId} and m.course_code=#{courseCode} "+
            "<if test='keyword!=null and keyword.trim() !=&quot;&quot;'> and (q.intro like CONCAT('%',#{keyword},'%') or q.content like CONCAT('%',#{keyword},'%') or q.answer like CONCAT('%',#{keyword},'%')) </if>" +
            "</script>"
    )
    public List<Question> query(@Param("specialityId") Integer specialityId, @Param("courseCode") String courseCode, @Param("keyword") String keyword);
}
