package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.Speciality;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

@Mapper
public interface SpecialityMapper extends BaseMapper<Speciality> {

    Speciality selectByIdFull(@Param("id") Integer id);
}
