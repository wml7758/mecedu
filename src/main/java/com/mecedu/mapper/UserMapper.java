package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.User;
import org.apache.ibatis.annotations.*;

import java.util.List;

@Mapper
public interface UserMapper extends BaseMapper<User> {

    @Update("update mecedu_user set passwd=#{passwd} where id=${id}")
    Integer updateAcc(@Param("id") Long id, @Param("passwd") String passwd);

    @Select("select count(1) from mecedu_user where code=#{code} and passwd=#{passwd} ")
    Integer queryAccount(@Param("code") String code, @Param("passwd") String passwd);

}
