package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.Dict;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface DictMapper extends BaseMapper<Dict> {

    @Select("select * from teach_dict where type_code=#{typeCode} and code=#{code}")
    Dict getDictById(@Param("typeCode") String typeCode, @Param("code") String code);
}
