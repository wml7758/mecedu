package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.AssessMatrix;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface AssessMatrixMapper extends BaseMapper<AssessMatrix> {


}
