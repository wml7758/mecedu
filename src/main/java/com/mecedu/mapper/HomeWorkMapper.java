package com.mecedu.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.Homework;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface HomeWorkMapper extends BaseMapper<Homework> {
}
