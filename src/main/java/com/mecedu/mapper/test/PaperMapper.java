package com.mecedu.mapper.test;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.test.Paper;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface PaperMapper extends BaseMapper<Paper> {
}
