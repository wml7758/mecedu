package com.mecedu.mapper.test;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.mecedu.mode.test.Question;
import com.mecedu.mode.test.QuestionModule;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

@Mapper
public interface QuestionModuleMapper extends BaseMapper<QuestionModule> {

    @Select("select q.* from mecedu_question as q where type=#{type} and exists (select 1 from mecedu_question_module as qm where q.id=qm.question_id and qm.module_id=#{moduleId})")
    Page<Question> selectByModule(Page page, @Param("moduleId") Integer moduleId, @Param("type") String type);
}
