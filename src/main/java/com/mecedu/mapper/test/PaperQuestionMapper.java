package com.mecedu.mapper.test;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.mecedu.mode.test.PaperQuestion;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

@Mapper
public interface PaperQuestionMapper extends BaseMapper<PaperQuestion> {

    @Select("select q.type as type,count(1) as count, sum(qm.score) as score from mecedu_question as q " +
            "inner join mecedu_question_module as qm on q.id=qm.question_id " +
            "where exists(select 1 from mecedu_paper_question as pq where pq.paper_id=#{pageId} and q.id=pq.question_id) " +
            "group by q.type")
    List<Map<String,Object>> pageSummarize(@Param("pageId") Integer pageId);

}
