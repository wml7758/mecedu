package com.mecedu.configuration;

import com.google.gson.Gson;
import org.apache.tomcat.util.security.MD5Encoder;
import org.junit.Test;
import org.springframework.util.DigestUtils;
import sun.security.provider.MD5;

import java.util.HashMap;
import java.util.Map;

public class ConstanceUtils {

    public static final Map<String, String> typeCodeName = new HashMap<>();
    static{
        typeCodeName.put("001","专业学制");
        typeCodeName.put("002","专业类别");
        typeCodeName.put("003","政治面貌");
        typeCodeName.put("004","研究方向");
        typeCodeName.put("005","授课学期");
        typeCodeName.put("006","授课学年");
        typeCodeName.put("007","试题类型");
        typeCodeName.put("008","入学年份");
        typeCodeName.put("009","课程性质");
        typeCodeName.put("010","课程文档类型");
        typeCodeName.put("011","考试类别");
        typeCodeName.put("012","教学方式");
        typeCodeName.put("013","教师职称");
        typeCodeName.put("014","学科门类");
    }

    /*默认密码*/
    public static final String PWD_DEFAULT = DigestUtils.md5DigestAsHex("12345".getBytes());

    public static final String encodePwd(String source){
        return DigestUtils.md5DigestAsHex(source.getBytes());
    }

    /*学期*/
    public static final String DICT_PEROID_SPRING="01";
    public static final String DICT_PEROID_FALL="02";

    /*员工类别*/
    public static final String USER_TYPE_TEACHER="1";
    public static final String USER_TYPE_STUDENT="2";
    public static final String USER_TYPE_ZHUREN="3";//主任
    public static final String USER_TYPE_MASTER="4";//院长
    public static final String USER_TYPE_ADMIN="5";//管理员


    /*教师与课程的关系*/
    public static final Integer COURSE_TEACHER_ROLE_CHARGE=1;/*责任教师*/
    public static final Integer COURSE_TEACHER_ROLE_TEACH=2;/*授课教师*/

    @Test
    public void test(){
        String s = DigestUtils.md5DigestAsHex("12456".getBytes());
        System.out.println(s);
    }

    public static String toJson(Object obj){
        return new Gson().toJson(obj);
    }
}
