package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;
import org.springframework.web.bind.annotation.GetMapping;

@TableName("mecedu_course_matrix")
@Setter
@Getter
public class CourseMatrix {

    @TableId(type= IdType.AUTO)
    private Integer id;

    private String codeCourse;
    private String codeIndicator;

    /*关联关系: H|M|L*/
    private String rel;

    private Float weight;

    private Integer specialityId;
}
