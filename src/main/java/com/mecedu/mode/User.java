package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_user")
@Setter
@Getter
public class User {

    @TableId(type= IdType.AUTO)
    private Integer id;
    private String name;
    private Integer sex;
    /*工号*/
    private String code;
    /*职称*/
    private String zhicheng;
    /*政治面貌*/
    private String zhengzhi;
    /*专业*/
    private String zhuanye;
    /*研究方向*/
    private String yanjiu;
    /*出生*/
    private String birth;
    /*员工类别  1:教师，2学生，3系主任，4主管院长，5系统维护*/
    private String type;
    /*电话*/
    private String phone;

    /*密码*/
    private String passwd;
}
