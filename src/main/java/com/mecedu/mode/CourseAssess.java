package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_course_assess")
@Getter
@Setter
public class CourseAssess {

    @TableId(type= IdType.AUTO)
    private Integer id ;

    private String courseCode;
    private Integer specialityId;

    private String segment;
    private String content;


}
