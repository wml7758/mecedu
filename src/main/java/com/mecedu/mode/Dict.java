package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.HashMap;
import java.util.Map;

@TableName("teach_dict")
@Data
public class Dict {

    @TableId(type= IdType.AUTO)
    private Long id;

    private String typeCode;

    @TableField(exist = false)
    private String typeName;

    private String code;

    private String name;

    private Integer ord;

}
