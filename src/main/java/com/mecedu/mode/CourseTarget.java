package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_course_target")
@Setter
@Getter
public class CourseTarget {

    @TableId(type= IdType.AUTO)
    private Integer id;

    private String courseCode;

    private String title;

    private String content;

    private String remark;

    private Integer specialityId;

}
