package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_module_matrix")
@Setter
@Getter
public class ModuleMatrix {

    @TableId(type= IdType.AUTO)
    private Integer id;

    private Integer moduleId;
    private Integer targetId;

    private Float weight;
}
