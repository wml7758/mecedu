package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_assess_matrix")
@Setter
@Getter
public class AssessMatrix {

    @TableId(type = IdType.INPUT)
    private Long id;


    private Integer assessId;
    private Integer targetId;

    private Float weight;
}
