package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_course_module")
@Setter
@Getter
public class CourseModule {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private String courseCode;

    private Integer specialityId;

    private String name;

    private Float score;

    private Integer ord;

    private Float timeTeach;
    private Float timeQuestion;
    private Float timePc;
    private Float timeTest;
    private Float timePractice;
    private Float timeDiscuss;

    private String content;

    private String claim;
}
