package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_target_matrix")
@Setter
@Getter
public class TargetMatrix {

    @TableId(type= IdType.AUTO)
    private Integer id;

    private Integer targetId;

    private String indicatorCode;

    private Float weight;
}
