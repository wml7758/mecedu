package com.mecedu.mode.test;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("mecedu_paper_question")
public class PaperQuestion {

    @TableId(type= IdType.AUTO)
    private Integer id;

    private Integer paperId;

    private Integer questionId;

    private Integer ord;
}
