package com.mecedu.mode.test;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.handlers.FastjsonTypeHandler;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.List;

@Data
@Accessors(chain = true)
@TableName("mecedu_question")
public class Question {

    @TableId(type= IdType.AUTO)
    private Integer id;
    /*试题类型*/
    private String type;
    /*难度系数*/
    private String difficulty;

    private String intro;

    private String content;

    private String answer;
}

