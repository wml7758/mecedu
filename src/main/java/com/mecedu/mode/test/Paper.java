package com.mecedu.mode.test;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;

@TableName("mecedu_paper")
@Setter
@Getter
public class Paper {
    @TableId(type = IdType.INPUT)
    private Long id;

    private String name;
    private Integer courseId;
    private Integer userId;
    private Date updateTime;
}
