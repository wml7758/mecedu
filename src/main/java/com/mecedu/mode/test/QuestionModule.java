package com.mecedu.mode.test;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
@TableName("mecedu_question_module")
public class QuestionModule {

    @TableId(type= IdType.AUTO)
    private Integer id;

    private Integer questionId;
    private Integer moduleId;
    private Float score;
}
