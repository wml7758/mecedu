package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_indicator")
@Setter
@Getter
public class Indicator {

    @TableId(type= IdType.AUTO)
    private Integer id;

    private Integer pid;

    private String code;

    private String content;

    private Integer specialityId;
}
