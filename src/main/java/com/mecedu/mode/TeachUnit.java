package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * 教学单位
 */
@TableName("mecedu_teach_unit")
@Setter
@Getter
public class TeachUnit {
    @TableId(type= IdType.AUTO)
    private Long id;
    private String code;
    private String name;

}
