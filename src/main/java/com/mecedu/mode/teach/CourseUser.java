package com.mecedu.mode.teach;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_course_user")
@Getter
@Setter
public class CourseUser {

    @TableId(type = IdType.INPUT)
    private Long id;

    private Integer specialityId;
    private String courseCode;

    private Integer userId;

    private String role;
}
