package com.mecedu.mode.teach;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("mecedu_course_class_user")
public class CourseClassUser {

    @TableId(type = IdType.INPUT)
    private Long id;

    private Integer courseId;

    private String code;

    private String classIds;

    private String userIds;

}
