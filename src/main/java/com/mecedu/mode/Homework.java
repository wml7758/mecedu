package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_homework")
@Getter
@Setter
public class Homework {

    @TableId(type= IdType.AUTO)
    private Integer id;

    private Integer module_id;

    private String content;
    private String answer;

}
