package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Data
@TableName(value = "mecedu_course", resultMap = "CourseFull")
public class Course {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer specialityId;

    private Integer pid;

    private String name;

    private String nameEn;

    private Integer teachTime;

    private Integer experiTime;

    private String code;

    private Integer pcTime;

    private String adapt;

    private String xingzhi;

    private Integer credit;

    private String type;

    private Integer allTime;

    private String isCore;

    private String yearNo;

    private String periodNo;

    private String unitCode;

    private Integer ord;

    @TableField(exist = false)
    private List<CourseTarget> targets;
}

