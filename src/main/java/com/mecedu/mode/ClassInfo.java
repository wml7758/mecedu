package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

@TableName("mecedu_class_info")
@Getter
@Setter
public class ClassInfo {

    @TableId(type = IdType.AUTO)
    private Integer id;

    private Integer specialityId;
    private String code;
    private String name;
    private String serial;
    private Integer people;

}
