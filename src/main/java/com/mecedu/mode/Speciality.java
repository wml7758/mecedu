package com.mecedu.mode;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * 专业
 */
@TableName(value = "mecedu_speciality")
@Getter
@Setter
public class Speciality {

    @TableId(type=IdType.AUTO)
    private Integer id;

    private String name;
    private Integer year;
    private String schoolName;

    private String kind;
    @TableField(exist = false)
    private Dict kindDict;

    private String subject;
    @TableField(exist = false)
    private Dict subjectDict;


    private String degree;

    private String xuezhi;
    @TableField(exist = false)
    private Dict xuezhiDict;
}
